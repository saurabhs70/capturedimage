# README #

This README would normally document whatever steps are necessary to get your application up and running.

### It's Very issue in 4 simple image to add your controller ###

1.Add #import <CaptureFramework/CaptureFramework.h> a header file
2.Add permission for camera access in your .plist file  "NSCameraUsageDescription"
3.Add "CaptureCameradelegate" delegate to your view controller.
4.Add "Capturedimage"  method for your each controller where you want to access result of captured image.

### How to use ###

CamViewController *camview=[[CamViewController alloc]init];
camview.captureCameradelegate=self;
[camview attachToViewController:self withFrame:CGRectMake(0, 0, SCREENWIDTH, SCREENHEIGHT)];

//this is delegate to use captured image result
-(void)Capturedimage:(UIImage *)image
{

}
