//
//  CaptureFramework.h
//  CaptureFramework
//
//  Created by saurabh-pc on 18/02/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CaptureFramework.
FOUNDATION_EXPORT double CaptureFrameworkVersionNumber;

//! Project version string for CaptureFramework.
FOUNDATION_EXPORT const unsigned char CaptureFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CaptureFramework/PublicHeader.h>

#import <CaptureFramework/CamViewController.h>
