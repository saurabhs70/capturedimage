//
//  CamViewController.h
//  Cameraapp
//
//  Created by saurabh-pc on 18/02/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol CaptureCameradelegate<NSObject>
-(void)Capturedimage:(UIImage*)image;
@end
@interface CamViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
- (void)attachToViewController:(UIViewController *)vc withFrame:(CGRect)frame;
@property(nonatomic,weak)id<CaptureCameradelegate>captureCameradelegate;
@end
