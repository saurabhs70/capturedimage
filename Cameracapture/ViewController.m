//
//  ViewController.m
//  Cameracapture
//
//  Created by saurabh-pc on 18/02/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
   
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)bntCaptureClicked:(id)sender {
    CamViewController *camview=[[CamViewController alloc]init];
    camview.captureCameradelegate=self;
    [camview attachToViewController:self withFrame:CGRectMake(0, 0, SCREENWIDTH, SCREENHEIGHT)];
}
//Delegate for after captured camera
-(void)Capturedimage:(UIImage *)image
{
    
}

@end
