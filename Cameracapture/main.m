//
//  main.m
//  Cameracapture
//
//  Created by saurabh-pc on 18/02/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
