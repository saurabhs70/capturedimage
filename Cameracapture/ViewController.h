//
//  ViewController.h
//  Cameracapture
//
//  Created by saurabh-pc on 18/02/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//
#define SCREENHEIGHT  [UIScreen mainScreen].bounds.size.height
#define SCREENWIDTH  [UIScreen mainScreen].bounds.size.width

#import <UIKit/UIKit.h>
#import <CaptureFramework/CaptureFramework.h>
@interface ViewController : UIViewController<CaptureCameradelegate>


- (IBAction)bntCaptureClicked:(id)sender;
@end

